<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Portfolio;
use Image;

class PortfolioController extends Controller
{
    public function view(){
        $portfolios = Portfolio::all();
        return view('backend.portfolio.view_portfolio',compact('portfolios'));
    }

    public function add(){
        return view('backend.portfolio.add_portfolio');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
        ],[
            'name.required' => 'Portfolio Name is required',
            'title.required' => 'Portfolio Title name in required',
        ]);

        $data = new Portfolio();
        $data->name = $request->name;
        $data->title = $request->title;
        $data->description = $request->description;
        $image = $request->image;
        if($image){
            $img_one = uniqid().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(1020,519)->save('upload/PortfolioImg/'.$img_one);
            $data['image'] = 'upload/PortfolioImg/'.$img_one;
            $data->save();
        }

        $notification = array(
            'message' => 'Portfolio added successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('view_portfolio')->with($notification);
    }

    public function edit($id){
        $editData = Portfolio::find($id);
        return view('backend.portfolio.edit_portfolio',compact('editData'));
    }

    public function update(Request $request, $id){
        $data = Portfolio::find($id);
        $data->name = $request->name;
        $data->title = $request->title;
        $data->description = $request->description;
        $image = $request->image;

        $oldimg = $request->oldimg;
        if($image){
            $img_one = uniqid().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(1020,519)->save('upload/PortfolioImg/'.$img_one);
            $data['image'] = 'upload/PortfolioImg/'.$img_one;
            unlink($oldimg);
            $data->update();
        }

        $notification = array(
            'message' => 'Portfolio updated successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('view_portfolio')->with($notification);
    }

    public function delete($id){
        $deleteData = Portfolio::find($id);
        $oldimg = $deleteData->image;
        unlink($oldimg);
        Portfolio::find($id)->delete();

        $notification = array(
            'message' => 'Portfolio remove successfully',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    /*--------==========Frontend method for details===========-----------*/
    public function portDetails($id){
        $details = Portfolio::find($id);
        return view('frontend.singlepage.details',compact('details'));
    }

    public function homePortfolio(){
        $portfolios = Portfolio::all();
        return view('frontend.portfolio',compact('portfolios'));
    }


}
