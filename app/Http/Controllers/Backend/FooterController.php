<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Footer;

class FooterController extends Controller
{
    public function view(){
        $footers = Footer::all();
        return view('backend.footer.view_footer',compact('footers'));
    }

    public function add(){
        return view('backend.footer.add_footer');
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'phone' => 'required',
            'short_description' => 'required',
            'address'  => 'required',
            'email'    => 'required',
            'facebook' => 'required',
            'twitter'  => 'required',
            'linkedin' => 'required',
            'copyright' => 'required',
        ]);

        $data = new Footer();
        $data->phone = $request->phone;
        $data->short_description = $request->short_description;
        $data->address = $request->address;
        $data->email = $request->email;
        $data->facebook = $request->facebook;
        $data->twitter = $request->twitter;
        $data->linkedin = $request->linkedin;
        $data->copyright = $request->copyright;
        $data->save();

        $notification = array(
            'message' => 'Footer added successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('view_footer')->with($notification);

    }

    public function edit($id){
        $data = Footer::find($id);
        return view('backend.footer.edit_footer',compact('data'));
    }

    public function update(Request $request, $id){
        $data = Footer::find($id);
        $data->phone = $request->phone;
        $data->short_description = $request->short_description;
        $data->address = $request->address;
        $data->email = $request->email;
        $data->facebook = $request->facebook;
        $data->twitter = $request->twitter;
        $data->linkedin = $request->linkedin;
        $data->copyright = $request->copyright;
        $data->update();

        $notification = array(
            'message' => 'Footer update successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('view_footer')->with($notification);
    }

    public function delete($id){
        $data = Footer::find($id)->delete();
        $notification = array(
            'message' => 'Footer deleted successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('view_footer')->with($notification);
    }


}
