<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Home;
use Illuminate\Http\Request;
use Image;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Home::find(1);
        return view('backend.home.index',compact('data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Home::find($id);
        $data->title = $request->title;
        $data->sub_title = $request->sub_title;
        $data->video = $request->video;

        /********* start of without intervention image  ***********/
        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('upload/homeimg/'.$data->image));
            $filename =date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/homeimg/'),$filename);
            $data['image']= $filename;
        }
        /******* start of without intervention image  ************/

        $data->update();

        return redirect()->back()->with('success','Home data update successfully');


    }

    public function mainHome(){
        return view('frontend.index');
    }
    
}
