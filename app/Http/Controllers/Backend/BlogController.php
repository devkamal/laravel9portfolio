<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogCategory;
use Image;

class BlogController extends Controller
{

    public function view(){
        $blogs = Blog::orderBy('id','desc')->get();
       return view('backend.blog.index',compact('blogs'));
    }

    public function add(){
        $categories = BlogCategory::orderBy('blog_category','desc')->get();
        return view('backend.blog.add_blog',compact('categories'));
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'blog_category_id' => 'required',
            'title' => 'required',
            'tags' => 'required',
            'image' => 'required',
            'description' => 'required',
        ]);

        $data = new Blog();
        $data->blog_category_id = $request->blog_category_id;
        $data->title = $request->title;
        $data->tags = $request->tags;
        $data->description = $request->description;
        $image = $request->image;
        if($image){
            $imgone = uniqid().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(430,327)->save('upload/blogimg/'.$imgone);
            $data['image'] = 'upload/blogimg/'.$imgone;
            $data->save();
        }

        $notification = array(
            'message' => 'Blog added successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('index')->with($notification);
    }

    public function edit($id){
        $blogs = Blog::find($id);
        $categories = BlogCategory::orderBy('blog_category','desc')->get();
        return view('backend.blog.edit_blog',compact('blogs','categories'));
    }

    public function update(Request $request, $id){
       $oldimg = $request->oldimg;
       if($request->file('image')){
        $image = $request->file('image');
        $namegen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(850,430)->save('upload/blogimg/'.$namegen);
        $save_url = 'upload/blogimg/'.$namegen;

        Blog::findOrFail($id)->update([
            'blog_category_id' => $request->blog_category_id,
            'title' => $request->title,
            'tags' => $request->tags,
            'description' => $request->description,
            'image' => $save_url,
        ]);

        $notification = array(
            'message' => 'Blog updated successfully',
            'alert-type' => 'success',
        );

        return redirect()->route('index')->with($notification);

       }else{
            Blog::findOrFail($id)->update([
                'blog_category_id' => $request->blog_category_id,
                'title' => $request->title,
                'tags' => $request->tags,
                'description' => $request->description,
            ]);

            $notification = array(
                'message' => 'Blog updated without image',
                'alert-type' => 'success',
            );

            return redirect()->back()->with($notification);
       }

    }

    public function delete($id){
        $deleteData = Blog::find($id);
        $oldimg = $deleteData->image;
        unlink($oldimg);
        Blog::find($id)->delete();

        $notification = array(
            'message' => 'Blog remove successfully',
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    /*--------==========Frontend route for Blog details===========-----------*/
    public function blogDetails($id){
        $allBlogs = Blog::latest()->limit(5)->get();
        $blogs = Blog::findOrFail($id);
        $categories = BlogCategory::orderBy('blog_category','asc')->get();
        $categoryname = BlogCategory::findOrFail($id);
        return view('frontend.singlepage.blogdetail',compact('allBlogs','blogs','categories','categoryname'));
    }

    public function categoryBlog($id){
        $allBlogs = Blog::latest()->limit(5)->get();
        $categories = BlogCategory::orderBy('blog_category','asc')->get();
        $blogpost = Blog::where('blog_category_id',$id)->orderBy('id','desc')->get();
        $categoryname = BlogCategory::findOrFail($id);
        return view('frontend.singlepage.category_blog_detail',compact('blogpost','allBlogs','categories','categoryname'));
    }

    public function homeBlog(){
        $allBlogs = Blog::latest()->paginate(3);
        $categories = BlogCategory::orderBy('blog_category','asc')->get();
        return view('frontend.singlepage.home_blog',compact('allBlogs','categories'));
    }



}
