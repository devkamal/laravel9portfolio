<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class AdminController extends Controller
{
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        $notification = array(
            'message' => 'User logout successfully!',
            'alert-type' => 'success',
        );

        return redirect('/login')->with($notification);
    }

    public function profile(){
        $id = Auth::user()->id;
        $adminData = User::find($id);
        return view('admin.profile.view_profile',compact('adminData'));
    }

    public function editProfile(){
        $id = Auth::user()->id;
        $editData = User::find($id);
        return view('admin.profile.edit_profile',compact('editData'));
    }

    public function store(Request $request){
        $id = Auth::user()->id;
        $data = User::find($id);
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;

        if ($request->file('profile_image')) {
            $file = $request->file('profile_image');

            @unlink(public_path('upload/adminimg/'.$data->profile_image));

            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/adminimg'),$filename);
            $data['profile_image'] = $filename;
        }
        $data->save();
        
        $notification = array(
            'message' => 'Admin profile updated successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('admin_profile')->with($notification);
        
    }

    /*////////
     Change Password area 
     //*/

    public function changePassword(){
        return view('admin.password.change_password');
    }

    public function updatePassword(Request $request){
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

        $hashPassword = Auth::user()->password;
        if(Hash::check($request->oldpassword,$hashPassword)){
            $users = User::find(Auth::id());
            $users->password = bcrypt($request->new_password);
            $users->save();

            session()->flash('message','Password change successfully!');
            return redirect('logout');
        }else{
            session()->flash('message','Oops something went wrong!');
            return redirect()->back();
        }


    }



}
