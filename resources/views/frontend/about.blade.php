@php
    $abouts = App\Models\About::first();
    $multiimgs = App\Models\MultiImg::all();
@endphp
<section id="aboutSection" class="about">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <ul class="about__icons__wrap">
                    @foreach ($multiimgs as $img)
                        <li>
                            <img class="light" src="{{ asset($img->multi_img) }}" alt="XD">
                            <img class="dark" src="{{ asset($img->multi_img) }}" alt="XD">
                        </li>
                    @endforeach
                 
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="about__content">
                    <div class="section__title">
                        <span class="sub-title">01 - About me</span>
                        <h2 class="title">{{ $abouts->title }}</h2>
                    </div>
                    <div class="about__exp">
                        <div class="about__exp__icon">
                            <img src="{{ asset('frontend/assets/img/icons/about_icon.png')}}" alt="">
                        </div>
                        <div class="about__exp__content">
                            <p>{{ $abouts->awards }} </p>
                        </div>
                    </div>
                    <p class="desc">{!! Str::limit($abouts->description, 250) !!}</p>
                    <a href="about.html" class="btn">Download my resume</a>
                </div>
            </div>
        </div>
    </div>
</section>