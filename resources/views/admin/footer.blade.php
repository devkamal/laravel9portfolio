

    <footer class="footer mt-auto">
        <div class="copyright bg-white">
            <p>
            <script>document.write(new Date().getFullYear())</script> © KamalHossen.
            <a
                class="text-primary"
                href="http://www.kamalhossen.com/"
                target="_blank"
                >LaravelDeveloper</a
            >.
            </p>
        </div>
        <script>
            var d = new Date();
            var year = d.getFullYear();
            document.getElementById("copy-year").innerHTML = year;
        </script>
        </footer>

    </div>
</div>