@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<div class="page-content">
    <div class="container-fluid">
        <h3 class="text-center my-2">
            <a href="{{ route('admin_profile') }}">Back</a>
        </h3>
  
        <form action="{{ route('store_profile') }}" method="post" enctype="multipart/form-data">
            @csrf
        
           <div class="row">
            <div class="col-md-6">
                <label for="img">Profile Image</label>
                <input id="image" class="block mt-1 w-full" type="file" name="profile_image"/>

                <img id="showImg" src="{{ asset(!empty($editData->profile_image)? url('upload/adminimg/'.$editData->profile_image): url('upload/noimg.png')) }}" 
                alt="img" class="rounded-circle" style="height: 50px;width:50px">
            </div>
            <div class="col-md-6">
                <label for="name">Name</label>
                <input class="form-control" type="text" name="name" value="{{ $editData->name }}">
            </div>
           </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="email">Email</label>
                    <input class="form-control" type="text" name="email" value="{{ $editData->email }}">
                </div>
    
                <div class="col-md-6">
                    <label for="UserName">User Name</label>
                    <input class="form-control" type="text" name="username" value="{{ $editData->username }}">
                </div>
            </div>

           <div class="col-md-12 mt-4">
            <button class="btn btn-primary form-control">Update</button>
           </div>

        </form>
  

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImg').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>

@endsection