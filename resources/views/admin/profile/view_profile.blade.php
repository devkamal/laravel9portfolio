@extends('admin.master')
@section('content')

<div class="page-content">
    <div class="container-fluid">

        <div class="card" style="width: 18rem;">
            <center>
                <img src="{{ asset(!empty($adminData->profile_image)? url('upload/adminimg/'.$adminData->profile_image): url('upload/noimg.png')) }}" 
                class="card-img-top rounded-circle avatar-xl mt-4" alt="img" style="height: 60px;width:60px">
            </center>
           
            <div class="card-body">
                <h5 class="card-title">Name: {{ $adminData->name }}</h5>
                <h5>Email: {{ $adminData->email }}</h5>
                <h5>User Name: {{ $adminData->username }}</h5>
                <a href="{{ route('edit_profile') }}" class="btn btn-primary">Edit Profile</a>
            </div>
        </div>

    </div>
</div>

@endsection