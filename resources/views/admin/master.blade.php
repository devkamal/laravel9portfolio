<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin LAravel Dashboard" name="description" />
        <meta content="Themesdesign" name="author" />
     

        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
        <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />
      
        <!-- PLUGINS CSS STYLE -->
        <link href="{{ asset('backend/assets/plugins/toaster/toastr.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('backend/assets/plugins/nprogress/nprogress.css') }}" rel="stylesheet" />
        <link href="{{ asset('backend/assets/plugins/flag-icons/css/flag-icon.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
        <link href="{{ asset('backend/assets/plugins/ladda/ladda.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('backend/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('backend/assets/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
      
        <!-- SLEEK CSS -->
        <link id="sleek-css" rel="stylesheet" href="{{ asset('backend/assets/css/sleek.css') }}" />
      
        <!-- FAVICON -->
        <link href="{{ asset('backend/assets/img/favicon.png') }}" rel="shortcut icon" />
     
        <script src="{{ asset('backend/assets/plugins/nprogress/nprogress.js') }}"></script>

        <!-- =========TagsInput Bootstrap========== -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha512-wu4jn1tktzX0SHl5qNLDtx1uRPSj+pm9dDgqsrYUS16AqwzfdEmh1JR8IQL7h+phL/EAHpbBkISl5HXiZqxBlQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      
    </head>

<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    
      @include('admin.leftsidebar')

      @include('admin.header')

      @yield('content')

      @include('admin.footer')



      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
      <script src="{{ asset('backend/assets/plugins/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/toaster/toastr.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/slimscrollbar/jquery.slimscroll.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/charts/Chart.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/ladda/spin.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/ladda/ladda.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery-mask-input/jquery.mask.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/select2/js/select2.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-world-mill.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/daterangepicker/moment.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jekyll-search.min.js') }}"></script>
      <script src="{{ asset('backend/assets/js/sleek.js') }}"></script>
      <script src="{{ asset('backend/assets/js/chart.js') }}"></script>
      <script src="{{ asset('backend/assets/js/date-range.js') }}"></script>
      <script src="{{ asset('backend/assets/js/map.js') }}"></script>
      <script src="{{ asset('backend/assets/js/custom.js') }}"></script>

      <!-----------Tinymce----------------->
      <script src="{{ asset('backend/assets/js/tinymce.js') }}"></script>
      <script src="{{ asset('backend/assets/js/form-editor.init.js') }}"></script>

            <!-- =========TagsInput Bootstrap========== -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


      <!-- Tostr js -->
      <script>
          @if(Session::has('message'))
          var type = "{{ Session::get('alert-type','info') }}"
          switch(type){
            case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
            case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
            case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
            case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
          }
          @endif
        </script>


        <!-------Sweet Alert-------->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <!-------sweet alert delete------->
        <script type="text/javascript">
            $(function(){
                $(document).on('click','#delete',function(e){
                    e.preventDefault();
                    var link = $(this).attr("href");
                    Swal.fire({
                    title: 'Are you sure?',
                    text: "Delete this data!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = link;
                        Swal.fire(
                        'Deleted!',
                        'Your file has been deleted successfully.',
                        'success'
                        )
                    }
                    });
                });
            });
        </script>

{{--============= Strat Tinymce Editor ============--}}
  <script src="https://cdn.tiny.cloud/1/jb1i1wz0jg4ioncohlcob5i938k9k1gy3flf6bps2neguv18/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
  {{--============= End Tinymce Editor ============--}}


  {{--============= Start of JQuery Validation ============--}}
  <script src="{{ asset('backend/assets/jquery-validation/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('backend/assets/jquery-validation/additional-methods.min.js') }}"></script>
   
    <script>
    $(function () {
        $('#myForm').validate({
            rules: {
                phone: {
                    required: true,
                    phone: true,
                },
                address: {
                    required: true,
                    address: true,
                },
                short_description: {
                    required: true,
                    short_description: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                facebook: {
                    required: true,
                    facebook: true,
                },
                twitter: {
                    required: true,
                    twitter: true,
                },
                linkedin: {
                    required: true,
                    linkedin: true,
                },
                copyright: {
                    required: true,
                    copyright: true,
                },
            },
            messages: {
              phone: {
                    required: "Please enter a real phone number",
                },
                address: {
                    required: "Please enter a valid address",
                },
                image: {
                    required: "Please enter a email address",
                    email: "Please enter a vaild email address"
                },
                terms: "Please accept our terms"
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script> 
{{--============= End of JQuery Validation ============--}}



    </body>

</html>