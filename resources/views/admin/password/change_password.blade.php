@extends('admin.master')
@section('content')

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<div class="page-content">
    <div class="container-fluid">
        <h3 class="text-center">
            Change Password
        </h3>
@if (count($errors))
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        "{{ $error }}"
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endforeach
@endif

        <form action="{{ route('update_password') }}" method="post">
            @csrf

            <div class="row">
                <div class="col-md-4">
                    <label for="oldpassword">Current Password</label>
                    <input class="form-control" type="password" name="oldpassword">
                </div>

                <div class="col-md-4">
                    <label for="new_password">New Password</label>
                    <input class="form-control" type="password" name="new_password">
                </div>
    
                <div class="col-md-4">
                    <label for="confirm_password">Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password">
                </div>
            </div>

           <div class="col-md-12 mt-4">
            <button class="btn btn-primary form-control">Password Update</button>
           </div>

        </form>
  

    </div>
</div>



@endsection