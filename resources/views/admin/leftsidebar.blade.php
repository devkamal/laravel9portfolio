<div class="wrapper">
      

<aside class="left-sidebar bg-sidebar">
<div id="sidebar" class="sidebar sidebar-with-footer">
  <!-- Aplication Brand -->
  <div class="app-brand">
    <a href="{{ url('/dashboard') }}">
        <div class="">
            <img src="{{ asset(!empty(Auth::user()->profile_image)? url('upload/adminimg/'.Auth::user()->profile_image) : url('upload/noimg.png')) }}" alt="img" class="avatar-md rounded-circle"
            style="width: 50px;height:50px">
        </div>
      <span class="brand-name">{{ Auth::user()->name }}</span>
    </a>
  </div>
  <!-- begin sidebar scrollbar -->
  <div class="sidebar-scrollbar">

    <!-- sidebar menu -->
    <ul class="nav sidebar-inner" id="sidebar-menu">
      

      
        <li  class="has-sub active expand" >
          <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#dashboard"
            aria-expanded="false" aria-controls="dashboard">
            <i class="mdi mdi-view-dashboard-outline"></i>
            <span class="nav-text">Dashboard</span> <b class="caret"></b>
          </a>
          <ul  class="collapse show"  id="dashboard"
            data-parent="#sidebar-menu">
            <div class="sub-menu">
              
              
                
                  <li  class="active" >
                    <a class="sidenav-item-link" href="{{ route('view_home') }}">
                      <span class="nav-text">Home</span>
                      
                    </a>
                  </li>
              
            </div>
          </ul>
        </li>


        <li  class="has-sub active expand" >
          <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#about"
            aria-expanded="false" aria-controls="about">
            <i class="mdi mdi-folder-multiple-outline"></i>
            <span class="nav-text">About Page Setup</span> <b class="caret"></b>
          </a>
          <ul  class="collapse show"  id="about"
            data-parent="#sidebar-menu">
            <div class="sub-menu">
                
                  <li >
                    <a class="sidenav-item-link" href="{{ route('view_about') }}">
                      <span class="nav-text text-white">About</span>
                    </a>
                  </li>

                  <li>
                    <a class="sidenav-item-link" href="{{ route('multiImg') }}">
                      <span class="nav-text text-white">Add MultiImg</span>
                    </a>
                  </li>

                  <li>
                    <a class="sidenav-item-link" href="{{ route('all_img') }}">
                      <span class="nav-text text-white">All MultiImg</span>
                    </a>
                  </li>
              
            </div>
          </ul>
        </li>

        <li  class="has-sub active expand" >
          <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#portfolio"
            aria-expanded="false" aria-controls="portfolio">
            <i class="mdi mdi-folder-multiple-outline"></i>
            <span class="nav-text">Portfolio Page Setup</span> <b class="caret"></b>
          </a>
          <ul  class="collapse show"  id="portfolio"
            data-parent="#sidebar-menu">
            <div class="sub-menu">
                
                  <li >
                    <a class="sidenav-item-link" href="{{ route('view_portfolio') }}">
                      <span class="nav-text text-white">View Portfolio</span>
                    </a>
                  </li>
              
            </div>
          </ul>
        </li>

        <li  class="has-sub active expand" >
          <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#blogcategory"
            aria-expanded="false" aria-controls="blogcategory">
            <i class="mdi mdi-folder-multiple-outline"></i>
            <span class="nav-text">Blog Category</span> <b class="caret"></b>
          </a>
          <ul  class="collapse show"  id="blogcategory"
            data-parent="#sidebar-menu">
            <div class="sub-menu">
                
                  <li >
                    <a class="sidenav-item-link" href="{{ route('view_blog_category') }}">
                      <span class="nav-text text-white">View Blog Category</span>
                    </a>
                  </li>

                  <li >
                    <a class="sidenav-item-link" href="{{ route('index') }}">
                      <span class="nav-text text-white">View All Blog</span>
                    </a>
                  </li>

                  <li >
                    <a class="sidenav-item-link" href="{{ route('add_blog') }}">
                      <span class="nav-text text-white">Add Blog</span>
                    </a>
                  </li>
              
            </div>
          </ul>
        </li>


        <li  class="has-sub active expand" >
          <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#footer"
            aria-expanded="false" aria-controls="footer">
            <i class="mdi mdi-folder-multiple-outline"></i>
            <span class="nav-text">Footer</span> <b class="caret"></b>
          </a>
          <ul  class="collapse show"  id="footer"
            data-parent="#sidebar-menu">
            <div class="sub-menu">

                  <li >
                    <a class="sidenav-item-link" href="{{ route('view_footer') }}">
                      <span class="nav-text text-white">View Footer</span>
                    </a>
                  </li>
              
            </div>
          </ul>
        </li>

        <li  class="has-sub active expand" >
          <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#contact"
            aria-expanded="false" aria-controls="contact">
            <i class="mdi mdi-folder-multiple-outline"></i>
            <span class="nav-text">Contact</span> <b class="caret"></b>
          </a>
          <ul  class="collapse show"  id="contact"
            data-parent="#sidebar-menu">
            <div class="sub-menu">

                  <li >
                    <a class="sidenav-item-link" href="{{ route('view_contact') }}">
                      <span class="nav-text text-white">View Conatact</span>
                    </a>
                  </li>
              
            </div>
          </ul>
        </li>
      
    </ul>

  </div>


</div>
</aside>