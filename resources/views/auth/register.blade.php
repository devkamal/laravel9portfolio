
<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Portfolio Admin & Dashboard" name="description" />
        <meta content="Portfolio" name="author" />
            <!-- GOOGLE FONTS -->
            <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
            <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />
          
            <!-- PLUGINS CSS STYLE -->
            <link href="{{ asset('backend/assets/plugins/toaster/toastr.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/nprogress/nprogress.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/flag-icons/css/flag-icon.min.css') }}" rel="stylesheet"/>
            <link href="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/ladda/ladda.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
          
            <!-- SLEEK CSS -->
            <link id="sleek-css" rel="stylesheet" href="{{ asset('backend/assets/css/sleek.css') }}" />
          
            <!-- FAVICON -->
            <link href="{{ asset('backend/assets/img/favicon.png') }}" rel="shortcut icon" />
         
            <script src="{{ asset('backend/assets/plugins/nprogress/nprogress.js') }}"></script>
     

    </head>

    <body class="auth-body-bg">
        <div class="bg-overlay"></div>
        <div class="wrapper-page">
            <div class="container p-0">
                <div class="card">
                    <div class="card-body">
    
                        <div class="text-center mt-4">
                            <div class="mb-3">
                                <a href="index.html" class="auth-logo">
                                    <img src="{{ asset('backend/assets/images/logo-dark.png') }}" height="30" class="logo-dark mx-auto" alt="">
                                    <img src="{{ asset('backend/assets/images/logo-light.png') }}" height="30" class="logo-light mx-auto" alt="">
                                </a>
                            </div>
                        </div>
    
                        <h4 class="text-muted text-center font-size-18"><b>Register</b></h4>
    
                        <div class="p-3">
                        
                            <form class="form-horizontal mt-3" method="POST" action="{{ route('register') }}">
                                    @csrf
    
                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input class="form-control" type="text" name="name" id="name" required="" placeholder="Name">
                                    </div>
                                </div>
    
                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input name="username" id="username" class="form-control" type="text" required="" placeholder="Username">
                                    </div>
                                </div>

                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input name="email" id="email" class="form-control" type="email" required="" placeholder="Email">
                                    </div>
                                </div>
    
                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input name="password" id="password" class="form-control" type="password" required="" placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input name="password_confirmation" id="password_confirmation" class="form-control" type="password" required="" placeholder="Password Confirmation">
                                    </div>
                                </div>
    
                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="form-label ms-1 fw-normal" for="customCheck1">I accept <a href="#" class="text-muted">Terms and Conditions</a></label>
                                        </div>
                                    </div>
                                </div>
    
                                <div class="form-group text-center row mt-3 pt-1">
                                    <div class="col-12">
                                        <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Register</button>
                                    </div>
                                </div>
    
                                <div class="form-group mt-2 mb-0 row">
                                    <div class="col-12 mt-3 text-center">
                                        <a href="{{ route('login') }}" class="text-muted">Already have account?</a>
                                    </div>
                                </div>
                            </form>
                            <!-- end form -->
                        </div>
                    </div>
                    <!-- end cardbody -->
                </div>
                <!-- end card -->
            </div>
            <!-- end container -->
        </div>
        <!-- end -->
        

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
        <script src="{{ asset('backend/assets/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/toaster/toastr.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/slimscrollbar/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/charts/Chart.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/ladda/ladda.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jquery-mask-input/jquery.mask.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/select2/js/select2.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-world-mill.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/daterangepicker/moment.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jekyll-search.min.js') }}"></script>
        <script src="{{ asset('backend/assets/js/sleek.js') }}"></script>
        <script src="{{ asset('backend/assets/js/chart.js') }}"></script>
        <script src="{{ asset('backend/assets/js/date-range.js') }}"></script>
        <script src="{{ asset('backend/assets/js/map.js') }}"></script>
        <script src="{{ asset('backend/assets/js/custom.js') }}"></script>

    </body>
</html>
