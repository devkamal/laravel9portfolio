
<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Portfolio Admin & Dashboard" name="description" />
        <meta content="Portfolio" name="author" />
            <!-- GOOGLE FONTS -->
            <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
            <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />
          
            <!-- PLUGINS CSS STYLE -->
            <link href="{{ asset('backend/assets/plugins/toaster/toastr.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/nprogress/nprogress.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/flag-icons/css/flag-icon.min.css') }}" rel="stylesheet"/>
            <link href="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/ladda/ladda.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
            <link href="{{ asset('backend/assets/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
          
            <!-- SLEEK CSS -->
            <link id="sleek-css" rel="stylesheet" href="{{ asset('backend/assets/css/sleek.css') }}" />
          
            <!-- FAVICON -->
            <link href="{{ asset('backend/assets/img/favicon.png') }}" rel="shortcut icon" />
         
            <script src="{{ asset('backend/assets/plugins/nprogress/nprogress.js') }}"></script>
     

    </head>

    <body class="auth-body-bg">
        <div class="bg-overlay"></div>
        <div class="wrapper-page">
            <div class="container p-0">
                <div class="card">
                    <div class="card-body">

                        <div class="text-center mt-4">
                            <div class="mb-3">
                                <a href="index.html" class="auth-logo">
                                    <img src="{{ asset('backend/assets/images/logo-dark.png') }}" height="30" class="logo-dark mx-auto" alt="">
                                    <img src="{{ asset('backend/assets/images/logo-light.png') }}" height="30" class="logo-light mx-auto" alt="">
                                </a>
                            </div>
                        </div>
    
                        <h4 class="text-muted text-center font-size-18"><b>Sign In</b></h4>
    
                        <div class="p-3">
                           
                            <form class="form-horizontal mt-3" method="POST" action="{{ route('login') }}">
                                    @csrf
    
                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input name="username" id="username" class="form-control" type="text" required="" placeholder="Username">
                                    </div>
                                </div>
    
                                <div class="form-group mb-3 row">
                                    <div class="col-12">
                                        <input name="password" id="password" class="form-control" type="password" required="" placeholder="Password">
                                    </div>
                                </div>
    
                                <div class="form-group mb-3 text-center row mt-3 pt-1">
                                    <div class="col-12">
                                        <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>
    
                                <div class="form-group mb-0 row mt-2">
                                    <div class="col-sm-7 mt-3">
                                        <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                    </div>
                                    <div class="col-sm-5 mt-3">
                                        <a href="{{ route('register') }}" class="text-muted"><i class="mdi mdi-account-circle"></i> Create an account</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end -->
                    </div>
                    <!-- end cardbody -->
                </div>
                <!-- end card -->
            </div>
            <!-- end container -->
        </div>
        <!-- end -->

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
        <script src="{{ asset('backend/assets/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/toaster/toastr.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/slimscrollbar/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/charts/Chart.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/ladda/ladda.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jquery-mask-input/jquery.mask.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/select2/js/select2.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jvectormap/jquery-jvectormap-world-mill.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/daterangepicker/moment.min.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('backend/assets/plugins/jekyll-search.min.js') }}"></script>
        <script src="{{ asset('backend/assets/js/sleek.js') }}"></script>
        <script src="{{ asset('backend/assets/js/chart.js') }}"></script>
        <script src="{{ asset('backend/assets/js/date-range.js') }}"></script>
        <script src="{{ asset('backend/assets/js/map.js') }}"></script>
        <script src="{{ asset('backend/assets/js/custom.js') }}"></script>
  


        <!-- Tostr js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            @if(Session::has('message'))
            var type = "{{ Session::get('alert-type','info') }}"
            switch(type){
              case 'info':
              toastr.info("{{ Session::get('message') }}");
              break;
              case 'success':
              toastr.success("{{ Session::get('message') }}");
              break;
              case 'warning':
              toastr.warning("{{ Session::get('message') }}");
              break;
              case 'error':
              toastr.error("{{ Session::get('message') }}");
              break;
            }
            @endif
          </script>

    </body>
</html>
