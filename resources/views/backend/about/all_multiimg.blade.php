@extends('admin.master')
@section('content')


<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>SL:</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($allimgs as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>
                                <img src="{{ asset($item->multi_img) }}" alt="img" width="50px;">
                            </td>
                            <td>
                                <a title="Edit" class="btn btn-primary" href="{{ route('edit-multiimg',$item->id) }}">Edit</a>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_img',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        

@endsection
