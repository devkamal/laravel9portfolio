@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


@if (session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
@endif

<div class="container mt-4">
    <form action="{{ route('store_multiimg') }}" method="post" enctype="multipart/form-data">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="multi_img">Image</label>
                <input type="file" multiple="" name="multi_img[]" id="image" class="form-control">
                @error('multi_img')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                <div>
                    <img id="showImg" src="{{ url('upload/noimg.png') }}" alt="img" style="height: 50px;width:50px">
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>




<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImg').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection
