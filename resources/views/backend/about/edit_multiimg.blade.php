@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h2>
                <a href="{{ route('all_img') }}">Back</a>
            </h2>
        </div>
    </div>
    <form action="{{ route('update_multiimg',$editData->id) }}" method="post" enctype="multipart/form-data">
        @csrf 

        <input type="hidden" name="oldimg" value="{{ $editData->multi_img }}">

        <div class="row">
            <div class="col-md-12">
                <label for="multi_img">Multi Image Update</label>
                <input type="file" name="multi_img" id="image" class="form-control" value="{{ $editData->multi_img }}">
            </div>
            <div>
                <img id="showImg" src="{{ asset($editData->multi_img), url('upload/noimg.png')}}" 
                alt="img" class="rounded-circle" style="height: 50px;width:50px">
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Update</button>
            </div>
        </div>
    </form>
</div>




<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImg').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection
