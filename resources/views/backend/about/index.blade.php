@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


@if (session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
@endif

<div class="container mt-4">
    <form action="{{ route('update_about',$data->id) }}" method="post" enctype="multipart/form-data">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="title">Title</label>
                <input type="text" name="title" value="{{ $data->title }}" class="form-control">
                @error('title')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-md-12">
                <label for="sub_title">Award</label>
                <input type="text" name="awards" value="{{ $data->awards }}" class="form-control">
                @error('awards')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-md-12">
                <label for="sub_title">Descriptions</label>
              
                <textarea name="description" aria-hidden="true" class="form-control">
                    {!! $data->description !!}
                </textarea>
                @error('description')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                
            </div>
            <div class="col-md-12">
                <label for="image">Image</label>
                <input type="file" name="image" id="image" value="{{ $data->image }}" class="form-control">
                @error('image')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                <div>
                    <img id="showImg" src="{{ asset(!empty($data->image)? url('upload/aboutimg/'.$data->image): url('upload/noimg.png')) }}" 
                    alt="img" class="rounded-circle" style="height: 50px;width:50px">
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Update</button>
            </div>
        </div>
    </form>
</div>



{{--============= Strat Tinymce Editor ============--}}
  <script src="https://cdn.tiny.cloud/1/jb1i1wz0jg4ioncohlcob5i938k9k1gy3flf6bps2neguv18/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
  {{--============= End Tinymce Editor ============--}}




<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImg').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>



@endsection
