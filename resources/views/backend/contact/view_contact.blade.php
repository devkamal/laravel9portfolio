@extends('admin.master')
@section('content')


<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th> SL: </th>
                        <th>Contact Name</th>
                        <th>Contact Email</th>
                        <th>Contact Phone</th>
                        <th>Contact Subject</th>
                        <th>Contact Message</th>
                        <th>Contact Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($contacts as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->subject }}</td>
                            <td>{{ $item->message }}</td>
                            <td>{{ Carbon\Carbon::parse($item->created_at)->diffforHumans() }}</td>
                            <td>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_contact',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        

@endsection
