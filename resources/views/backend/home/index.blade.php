@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


<div class="container mt-4">
    <form action="{{ route('update',$data->id) }}" method="post" enctype="multipart/form-data">
        @csrf 

        
        <div class="row">
            <div class="col-md-3">
                <label for="title">Title</label>
                <input type="text" name="title" value="{{ $data->title }}" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="sub_title">SubTitle</label>
                <input type="text" name="sub_title" value="{{ $data->sub_title }}" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="video">Video</label>
                <input type="text" name="video" value="{{ $data->video }}" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="image">Image</label>
                <input type="file" name="image" id="image" value="{{ $data->image }}" class="form-control">
                <div>
                    <img id="showImg" src="{{ asset(!empty($data->image)? url('upload/homeimg/'.$data->image): url('upload/noimg.png')) }}" 
                    alt="img" class="rounded-circle" style="height: 50px;width:50px">
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Update</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change(function(e){
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImg').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script>

@endsection