@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

@if (session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
@endif

<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_portfolio') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('store_portfolio') }}" method="post" enctype="multipart/form-data">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="name">Portfolio Name</label>
                <input type="text" name="name" placeholder="Add your portfolio name" class="form-control">
                @error('name')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="title">Portfolio Title</label>
                <input type="text" name="title" placeholder="Add your portfolio title" class="form-control">
                @error('title')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="dsc">Portfolio Descriptions</label>
              
                <textarea name="description" aria-hidden="true" class="form-control"></textarea>
                @error('description')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                
            </div>

            <div class="col-md-12">
                <label for="image">Portfolio Image</label>
                <input type="file" name="image" id="image" class="form-control">
                @error('image')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                <img id="showImg" src="{{url('upload/noimg.png')}}" alt="img" width="50px;">
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>





  <script type="text/javascript">
        $(document).ready(function(){
            $('#image').change(function(e){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#showImg').attr('src',e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
  </script>


@endsection
