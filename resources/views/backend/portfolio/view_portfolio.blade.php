@extends('admin.master')
@section('content')


<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('add_portfolio') }}" class="btn btn-primary">Add Portfolio</a>
            </h3>
        </div>
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>SL:</th>
                        <th>Portfolio Name</th>
                        <th>Portfolio Title</th>
                        <th>Portfolio Dscription</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($portfolios as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{!! $item->description !!}</td>
                            <td>
                                <img src="{{ asset($item->image) }}" alt="img" width="150px;">
                            </td>
                            <td>
                                <a title="Edit" class="btn btn-primary" href="{{ route('edit_portfolio',$item->id) }}">Edit</a>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_portfolio',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        

@endsection
