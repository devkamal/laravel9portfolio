@extends('admin.master')
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

@if (session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
@endif

<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_portfolio') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('update_portfolio',$editData->id ) }}" method="post" enctype="multipart/form-data">
        @csrf 

        <input type="hidden" name="oldimg" value="{{ $editData->image }}">
        <div class="row">
            <div class="col-md-12">
                <label for="name">Portfolio Name</label>
                <input type="text" name="name" value="{{ $editData->name }}" class="form-control">
                @error('name')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="title">Portfolio Title</label>
                <input type="text" name="title" value="{{ $editData->title }}" class="form-control">
                @error('title')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="dsc">Portfolio Descriptions</label>
              
                <textarea name="description" aria-hidden="true" class="form-control">
                    {!! $editData->description !!}
                </textarea>
                @error('description')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                
            </div>

            <div class="col-md-12">
                <label for="image">Portfolio Image</label>
                <input type="file" name="image" id="image" class="form-control">
                @error('image')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                <img id="showImg" src="{{ asset($editData->image), url('upload/noimg.png') }}" 
                alt="img" width="50px;">
            </div>
            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>



{{--============= Strat Tinymce Editor ============--}}
  <script src="https://cdn.tiny.cloud/1/jb1i1wz0jg4ioncohlcob5i938k9k1gy3flf6bps2neguv18/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
  {{--============= End Tinymce Editor ============--}}

  <script type="text/javascript">
        $(document).ready(function(){
            $('#image').change(function(e){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#showImg').attr('src',e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
  </script>


@endsection
