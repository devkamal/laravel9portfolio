@extends('admin.master')
@section('content')

<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_footer') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('update_footer',$data->id) }}" method="post" enctype="multipart/form-data" id="myForm">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="phone">Contact Number</label>
                <input type="text" name="phone" class="form-control" value="{{ $data->phone }}">
            </div>

            <div class="col-md-12">
                <label for="address">Address</label>
                <input type="text" name="address" value="{{ $data->address }}" class="form-control">
            </div>

            <div class="col-md-12">
                <label for="dsc">Short Descriptions</label>
              
                <textarea name="short_description" aria-hidden="true" class="form-control">
                    {!! $data->short_description !!}
                </textarea>
                
            </div>

            <div class="col-md-12">
                <label for="email">Email</label>
                <input type="text" name="email" value="{{ $data->email }}" class="form-control">
            </div>

            <div class="col-md-12">
                <label for="facebook">Facebook</label>
                <input type="text" name="facebook" value="{{ $data->facebook }}" class="form-control">
                @error('facebook')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="twitter">Twitter</label>
                <input type="text" name="twitter" value="{{ $data->twitter }}" class="form-control">
            </div>

            <div class="col-md-12">
                <label for="linkedin">Linkedin</label>
                <input type="text" name="linkedin" value="{{ $data->linkedin }}" class="form-control">
            </div>

            <div class="col-md-12">
                <label for="copyright">Copyright</label>
                <input type="text" name="copyright" value="{{ $data->copyright }}" class="form-control">
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>


@endsection
