@extends('admin.master')
@section('content')

<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_footer') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('store_footer') }}" method="post" enctype="multipart/form-data" id="myForm">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="phone">Contact Number</label>
                <input type="text" name="phone" placeholder="Add your phone" class="form-control">
                @error('phone')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="address">Address</label>
                <input type="text" name="address" placeholder="Add your address" class="form-control">
                @error('address')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="dsc">Short Descriptions</label>
              
                <textarea name="short_description" aria-hidden="true" class="form-control"></textarea>
                @error('short_description')
                    <span style="color:red">{{ $message }}</span>
                @enderror
                
            </div>

            <div class="col-md-12">
                <label for="email">Email</label>
                <input type="text" name="email" placeholder="Add your email" class="form-control">
                @error('email')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="facebook">Facebook</label>
                <input type="text" name="facebook" placeholder="Add your facebook" class="form-control">
                @error('facebook')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="twitter">Twitter</label>
                <input type="text" name="twitter" placeholder="Add your twitter" class="form-control">
                @error('twitter')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="linkedin">Linkedin</label>
                <input type="text" name="linkedin" placeholder="Add your linkedin" class="form-control">
                @error('linkedin')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12">
                <label for="copyright">Copyright</label>
                <input type="text" name="copyright" placeholder="Add your copyright" class="form-control">
                @error('copyright')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>


@endsection
