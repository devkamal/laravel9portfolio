@extends('admin.master')
@section('content')


<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('add_footer') }}" class="btn btn-primary">Add Portfolio</a>
            </h3>
        </div>
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th> Phone </th>
                        <th>Short description</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Facebook</th>
                        <th>Twitter</th>
                        <th>Linkedin</th>
                        <th>Copyright</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($footers as $key => $item)
                        <tr>
                            <td>{{ $item->phone }}</td>
                            <td>{!! $item->short_description !!}</td>
                            <td>{{ $item->address }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->facebook }}</td>
                            <td>{{ $item->twitter }}</td>
                            <td>{{ $item->linkedin }}</td>
                            <td>{{ $item->copyright }}</td>
                            <td>
                                <a title="Edit" class="btn btn-primary" href="{{ route('edit_footer',$item->id) }}">Edit</a>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_footer',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        

@endsection
