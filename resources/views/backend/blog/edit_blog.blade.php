@extends('admin.master')
@section('content')

<!-- =========TagsInput Bootstrap========== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<style>
    .bootstrap-tagsinput .tag{
        margin-right: 1px;
        color:#ff0080;
        font-weight:700px;
    }
</style>

    <div class="container mt-4">
    <div class="row">
        <div class="col-md-12 d-flex">
            
            <p>Update Blogs</p>
            <p style="float:right">
                <a href="{{ route('index') }}">Back</a>
            </p>
            
        </div>
    </div>
    <form action="{{ route('update_blog',$blogs->id) }}" method="post" enctype="multipart/form-data">
        @csrf 

        <div class="row">
            <div class="col-md-6">
                <label for="blog_category_id">Blog category Name</label>
               <select name="blog_category_id" id="" class="form-control">
                    <option value="1">Select Category Name</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ $category->id == $blogs->blog_category_id ? "selected" : '' }} ?>
                        {{ $category->blog_category }}</option>
                    @endforeach
               </select>
            </div>

            <div class="col-md-6">
                <label for="title"> category Title</label>
                <input type="text" name="title" value="{{ $blogs->title }}" class="form-control">
                @error('title')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-6 mt-4">
                <label for="tags"> category Tags</label>
                <input type="text" name="tags" value="{{ $blogs->tags }}" class="form-control" value="home,tags" data-role="tagsinput">
            </div>


            <div class="col-md-6">
                <label for="image"> category Image</label>
                <input type="file" name="image" id="image" class="form-control">
                <img id="showImg" src="{{ asset($blogs->image), url('upload/noimg.png')}}" alt="img" width="50px;">
                <input type="hidden" name="oldimg" value="{{$blogs->image}}">
            </div>


            <div class="col-md-12">
                <label for="description"> category Descriptions</label>
                <textarea name="description" id="tinymce" class="form-control">
                {!! $blogs->description !!}
                </textarea>
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Update</button>
            </div>
       
    </form>
</div>

<script type="text/javascript">
        $(document).ready(function(){
            $('#image').change(function(e){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#showImg').attr('src',e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
</script>



@endsection