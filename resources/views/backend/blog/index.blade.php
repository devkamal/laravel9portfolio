@extends('admin.master')
@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-12 text-center py-4">
            <h2>All Blogs</h2>
        </div>
        <div class="col-md-12">
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <th>Sl:</th>
                    <th>Category Name</th>
                    <th>Category Title</th>
                    <th>Category Tags</th>
                    <th>Category Descriptions</th>
                    <th>Category Image</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($blogs as $key => $blog)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $blog['category']['blog_category'] }}</td>
                        <td>{{ $blog->title }}</td>
                        <td>{{ $blog->tags }}</td>
                        <td>{!! $blog->description !!}</td>
                        <td>
                            <img src="{{ asset($blog->image) }}" alt="img" width="100px">
                        </td>
                        <td>
                            <a href="{{ route('edit_blog',$blog->id) }}" class="btn btn-primary btn-sm" title="Edit">Edit</a>
                            <a href="{{ route('delete_blog',$blog->id) }}" id="delete" class="btn btn-danger btn-sm" title="Delete">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection