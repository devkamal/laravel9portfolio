@extends('admin.master')
@section('content')


<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('view_blog_category') }}">Back</a>
            </h3>
        </div>
    </div>
    <form action="{{ route('store_blog_category') }}" method="post" enctype="multipart/form-data">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="blog_category">Blog category Name</label>
                <input type="text" name="blog_category" placeholder="Add your blog category name" class="form-control">
                @error('blog_category')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Submit</button>
            </div>
        </div>
    </form>
</div>



@endsection
