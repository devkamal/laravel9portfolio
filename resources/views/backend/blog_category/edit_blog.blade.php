@extends('admin.master')
@section('content')


<div class="container mt-4">
    <div class="row">
        <div class="col-md-6">
            <h3>
                 <a href="{{ route('view_blog_category') }}">Back</a>
            </h3>
        </div>
        <div class="col-md-6">
            <h3 class="center">
                 Update Blog Category
            </h3>
        </div>
    </div>
    <form action="{{ route('update_blog_category',$editData->id) }}" method="post" enctype="multipart/form-data">
        @csrf 

        <div class="row">
            <div class="col-md-12">
                <label for="blog_category">Blog category Name</label>
                <input type="text" name="blog_category" value="{{ $editData->blog_category }}" class="form-control">
                @error('blog_category')
                    <span style="color:red">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-12 mt-4">
                <button class="btn btn-primary form-control">Update</button>
            </div>
        </div>
    </form>
</div>



@endsection
