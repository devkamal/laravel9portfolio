@extends('admin.master')
@section('content')


<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <a href="{{ route('add_blog_category') }}" class="btn btn-primary">Add Blog Category</a>
            </h3>
        </div>
        <div class="col-md-12">      
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>SL:</th>
                        <th>Blog Category Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blogs as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->blog_category }}</td>
                            <td>
                                <a title="Edit" class="btn btn-primary" href="{{ route('edit_blog_category',$item->id) }}">Edit</a>
                                <a title="Delete" class="btn btn-danger" href="{{ route('delete_blog_category',$item->id) }}"
                                    id="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        

@endsection
