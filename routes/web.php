<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Backend\HomeController;
use App\Http\Controllers\Backend\AboutController;
use App\Http\Controllers\Backend\PortfolioController;
use App\Http\Controllers\Backend\BlogCategoryController;
use App\Http\Controllers\Backend\BlogController;
use App\Http\Controllers\Backend\FooterController;
use App\Http\Controllers\Backend\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Route::get('/dashboard', function () {
    return view('admin.index');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
    Route::controller(AdminController::class)->group(function(){
        Route::get('/logout', 'destroy')->name('admin_logout');
        Route::get('/admin/profile', 'profile')->name('admin_profile');
        Route::get('/edit/profile', 'editProfile')->name('edit_profile');
        Route::post('/store/profile', 'store')->name('store_profile');

        Route::get('/change-passowrd','changePassword')->name('change_password');
        Route::post('/update-password','updatePassword')->name('update_password');
    });
});

Route::controller(HomeController::class)->group(function(){
    Route::get('/view/home', 'index')->name('view_home');
    Route::post('/update/{id}', 'update')->name('update');

    Route::get('/home', 'mainHome')->name('home');
});

Route::controller(AboutController::class)->group(function(){
    Route::get('/view-about', 'index')->name('view_about');
    Route::post('/update-about/{id}', 'update')->name('update_about');

    Route::get('/about-page','aboutPage')->name('about_page');

    Route::get('/multi-image', 'multiImg')->name('multiImg');
    Route::post('/add-multi-img', 'storeImg')->name('store_multiimg');

    Route::get('/all-img', 'allImg')->name('all_img');
 
    Route::get('/edit-multiimg/{id}', 'editMultiImg')->name('edit-multiimg');
    Route::post('/update-img/{id}', 'updateMultiImg')->name('update_multiimg');
    Route::get('/edit-img/{id}', 'deleteImg')->name('delete_img');
});

Route::controller(PortfolioController::class)->group(function(){
    Route::get('/portfolio-view', 'view')->name('view_portfolio');
    Route::get('/portfolio-add', 'add')->name('add_portfolio');
    Route::post('/portfolio-store', 'store')->name('store_portfolio');
    Route::get('/portfolio-edit/{id}', 'edit')->name('edit_portfolio');
    Route::post('/portfolio-update/{id}', 'update')->name('update_portfolio');
    Route::get('/portfolio-delete/{id}', 'delete')->name('delete_portfolio');

    /*--------==========Frontend route for details===========-----------*/
    Route::get('/portfolio-details/{id}', 'portDetails')->name('portfolio_details');

    Route::get('/home-portfolio','homePortfolio')->name('home_portfolio');
});

Route::controller(BlogCategoryController::class)->group(function(){
    Route::get('/view-blog-category', 'viewCate')->name('view_blog_category');
    Route::get('/add-blog-category', 'addCate')->name('add_blog_category');
    Route::post('/store-blog-category', 'storeCate')->name('store_blog_category');
    Route::get('/edit-blog-category/{id}', 'editCate')->name('edit_blog_category');
    Route::post('/update-blog-category/{id}', 'updateCate')->name('update_blog_category');
    Route::get('/delete-blog-category/{id}', 'deleteCate')->name('delete_blog_category');
});

Route::controller(BlogController::class)->group(function(){
    Route::get('/view-blog', 'view')->name('index');
    Route::get('/add-blog', 'add')->name('add_blog');
    Route::post('/store-blog', 'store')->name('store_blog');
    Route::get('/edit-blog/{id}', 'edit')->name('edit_blog');
    Route::post('/update-blog/{id}', 'update')->name('update_blog');
    Route::get('/delete-blog/{id}', 'delete')->name('delete_blog');

    /*--------==========Frontend route for Blog details===========-----------*/
    Route::get('/blog-details/{id}', 'blogDetails')->name('blog_details');

    Route::get('/category-blog/{id}','categoryBlog')->name('category_blog');

    Route::get('/blog','homeBlog')->name('home_blog');
    
});

Route::controller(FooterController::class)->group(function(){

    Route::get('/view-footer','view')->name('view_footer');
    Route::get('/add-footer','add')->name('add_footer');
    Route::post('/store-footer','store')->name('store_footer');
    Route::get('/edit-footer/{id}','edit')->name('edit_footer');
    Route::post('/update-footer/{id}','update')->name('update_footer');
    Route::get('/delete-footer/{id}','delete')->name('delete_footer');

});

Route::controller(ContactController::class)->group(function(){
    Route::get('/contact','contact')->name('contact_me');
    Route::post('/store','storeMsg')->name('store_message');

    Route::get('/contact-message','contactMsg')->name('view_contact');
    Route::get('/contact-delete/{id}','deleteMsg')->name('delete_contact');
});


